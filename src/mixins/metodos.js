import Vue from 'vue'
import store from '@/store'
import XLSX from 'xlsx';
import { mapGetters, mapActions } from 'vuex'

// const axios = require('axios');

export default {
    data() {
        return {}
    },

    computed: {
        ...mapGetters('Login', ['getdatosUsuario']),

        tamanioPantalla() {
            return this.$vuetify.breakpoint.height - 320
        },

        pantallaMaquilador() {
            return this.$vuetify.breakpoint.height - 120
        },

    },

    methods: {
        // ...mapActions('Listado',['buscar_productos_categoria','buscar_productos_subcategorias']),

        traerFechaActual() {
            var f = new Date();
            return f.getFullYear() + '-' + (f.getMonth() + 1 < 10 ? '0' + (f.getMonth() + 1) : f.getMonth() + 1) + '-' + (f.getDate() < 10 ? '0' + f.getDate() : f.getDate());
        },
        traerHoraActual() {
            var f = new Date();
            return (f.getHours() < 10 ? '0' + f.getHours() : f.getHours()) + ':' + (f.getMinutes() < 10 ? '0' + f.getMinutes() : f.getMinutes())
        },

        getAuthHeaders() {
            // Obtener el token de Vuex o localStorage
            const token = localStorage.getItem('EPIC_TOKEN');
            return {
              headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json'
              }
            };
        },
        // ciudades_por_codigo_postal(cp) {
        //     let config = {
        //         method: 'GET',
        //         url: 'https://geocodes.envia.com/zipcode/MX/' + cp,
        //         headers: {
        //             'Content-Type': 'application/json',
        //             'Authorization': 'Bearer 8df7dc4cb48b5c19d9e0c7f6cf07abdfdd6d2e2d90bc34b2711a85174ff087de'
        //         },
        //     };

        //     axios(config).then((response) => {
        //         this.colonias = response.data[0].suburbs
        //         this.ObjectDefault.municipio = response.data[0].locality
        //         this.ObjectDefault.estado = Object.values(response.data[0].state.code)[1];
        //     }).catch((error) => {
        //         console.log(error);
        //     });
        // },
        // generarEnvio(idpago, direccion_envio, productos) {

        //     let direccion = direccion_envio; // direccion de envio
        //     let contenido = '',
        //         peso = 0,
        //         alto = null,
        //         ancho = null,
        //         largo = null

        //     for (let i = 0; i < productos.length; i++) {
        //         contenido = contenido + productos[i].nomart + ', ';
        //         alto > productos[i].alto ? alto = alto : alto = productos[i].alto;

        //         if (productos[i].cantidad > 1) {
        //             peso = peso + (productos[i].peso * productos[i].cantidad);
        //             ancho = ancho + (productos[i].ancho * productos[i].cantidad);
        //         } else {
        //             peso = peso + productos[i].peso;
        //             ancho = ancho + productos[i].ancho;
        //         }
        //     }

        //     const body = {
        //         "origin": {
        //             "name": "GERARDO SANCHEZ VILLAREAL",
        //             "company": "GRUPO ACALG SA de CV",
        //             "email": "compras@acalg.com",
        //             "phone": "8125572046",
        //             "street": "Paseo de los estudiantes",
        //             "number": "2100",
        //             "district": "Cumbres 2do Sector",
        //             "city": "Monterrey",
        //             "state": "NL",
        //             "country": "MX",
        //             "postalCode": "64610",
        //             "reference": ""
        //         },

        //         "destination": {
        //             "name": direccion.nombre + ' ' + direccion.apellido, // NOMBRE DEL USUARIO
        //             "company": "", // COMAPÑIA N/A
        //             "email": direccion.email, // EMAIL DEL USUARIO
        //             "phone": direccion.telefono, // TELEFONO DEL USUARIO
        //             "street": direccion.calle,
        //             "number": direccion.numero,
        //             "district": direccion.colonia,
        //             "city": direccion.municipio,
        //             "state": direccion.estado,
        //             "country": "MX", // SIEMPRE MEXICO
        //             "postalCode": direccion.cp, // CP DE ENVIO
        //             "reference": ""
        //         },
        //         "packages": [{
        //             "content": contenido, // CONCATENAR NOMBRES ARTICULOS
        //             "amount": 1, // TOTAL DE TODOS LOS PRODUCTOS    
        //             "type": "box",
        //             "weight": peso, // SUMA DE TODOS 
        //             "insurance": 0,
        //             "declaredValue": 0,
        //             "weightUnit": "KG",
        //             "lengthUnit": "CM",
        //             "dimensions": {
        //                 "length": alto, // ALTURA MAYOR 
        //                 "width": ancho, // SUMAS DE TODOS LOS ANCHOS
        //                 "height": alto // ALTURAS LA MAYOR
        //             }
        //         }],
        //         "shipment": {
        //             "carrier": "noventa9Minutos",
        //             "service": "local_next_day",
        //             "type": 1
        //         },
        //         "settings": {
        //             "currency": "MXN",
        //             "printFormat": "PDF",
        //             "printSize": "PAPER_7X4.75",
        //             "cashOnDelivery": "0",
        //             "comments": ""
        //         }
        //     }

        //     let config = {
        //         method: 'POST',
        //         url: 'https://api.envia.com/ship/generate/',
        //         headers: {
        //             'Content-Type': 'application/json',
        //             'Authorization': 'Bearer 8df7dc4cb48b5c19d9e0c7f6cf07abdfdd6d2e2d90bc34b2711a85174ff087de'
        //         },
        //         data: body
        //     };

        //     /****************************************************************************************************************/
        //     // Aquí, antes que nada, vas a validar siiii en rastreos existe ya ese idpago, no vassss a generar el envíooooo
        //     /****************************************************************************************************************/
        //     return new Promise((resolve, reject) => {
        //         this.$http.get('obtener.rastreo/' + idpago).then(res => {
        //             // Evaluo si la respuesta viene con al menos 1 elemento.
        //             if (res.body.length) {
        //                 resolve({ estatus: false, mensaje: 'Este pedido ya se había generado anteriormente.' }) // Si la respuesta es true entonces retorno falso para lanzar alerta o retornar.
        //             } else { // Si la respuesta es false entonces paso a generar el envio.
        //                 // console.log('AL ENTRAR AQUI SE GENERA EL ENVIO');
        //                 // return; 

        //                 axios(config).then((res2) => {
        //                     // OBJETO DE PRUEBA
        //                     // const payload = new Object({
        //                     // 		idpago: "681744925-593ce90d-7d01-49f0-be7f-7e24012ff72c",
        //                     // 		carrier: "noventa9Minutos",
        //                     // 		currency: "MXN",
        //                     // 		currentBalance: "22.44",
        //                     // 		label: "https://s3.us-east-2.amazonaws.com/enviapaqueteria/uploads/noventa9Minutos/172487986883832616e56fcde205.pdf",
        //                     // 		service: "local_next_day",
        //                     // 		totalPrice: "75.4",
        //                     // 		trackUrl: "https://envia.com/rastreo?label=1724879868&cntry_code=mx",
        //                     // 		trackingNumber: "1724879868"
        //                     // });

        //                     if (res2.data.meta == 'generate') {
        //                         let envio = {...res2.data.data[0], idpago: idpago };
        //                         // let envio = { ...payload, idpago : idpago}; // ENVIO DE PRUEBA
        //                         // Insertart los datos de rastreo que serían los de this.envíoY como dato para relacionar todo, vas a guardar tambien el id idpago
        //                         this.$http.post('agregar.rastreo', envio).then(res3 => {
        //                             // console.log('res3', res3.body);
        //                             resolve({ estatus: true, mensaje: 'Envio generado correctamente.', data: envio });

        //                         }).catch(err3 => {
        //                             reject({ estatus: false, mensaje: 'Tuvimos un problema al guardar la informacion del envio.' });
        //                             console.error('err3', err3);
        //                         });

        //                     } else {
        //                         console.error('Error al generar el envío, intentalo mas tarde.');
        //                     }

        //                 }).catch((error) => {
        //                     reject({ estatus: false, mensaje: 'Error al generar el envío, intentalo mas tarde.' })
        //                     console.log(error);
        //                 });
        //             }

        //         }).catch(error => {
        //             console.log('ERROR RASTREO', error);
        //         });
        //     })
        // },

        consultar_bodegas() {
            return new Promise((resolve) => {
                this.$http.post('obtener.bodegas.activas').then((response) => {
                    resolve(response.body)
                }).catch(error => {
                    console.log('error consultar_maquiladores', error)
                })
            })
        },

        consultar_tipo_usuario(idBodega, nivel) {
            return new Promise((resolve) => {
                const payload = {
                    idbodegas: idBodega,
                    idniveles: nivel
                }
                this.$http.post('obtener.tipo.usuario', payload).then((response) => {
                    resolve(response.body)
                }).catch(error => {
                    console.log('error consultar_tipo.usuario', error)
                })
            })
        },

        consultar_costo_envio() {
            return new Promise((resolve) => {
                this.$http.get('obtener.costo.envio').then((response) => {
                    resolve(response.body)
                }).catch(error => {
                    console.log('error consultar_maquiladores', error)
                })
            })
        },

        consultar_niveles() {
            return new Promise((resolve) => {
                this.$http.get('obtener.niveles').then((response) => {
                    resolve(response.body)
                }).catch(error => {
                    console.log('error consultar_niveles', error)
                })
            })
        },

        consultar_usuarios_administrativos() {
            return new Promise((resolve) => {
                this.$http.get('obtener.usuarios.admin').then((response) => {
                    resolve(response.body)
                }).catch(error => {
                    console.log('error consultar_usuarios_administrativos', error)
                })
            })
        },

        consultar_productos() {
            return new Promise((resolve) => {
                this.$http.get('articulos.list').then((response) => {
                    resolve(response.body)
                }).catch(error => {
                    console.log('error consultar_productos', error)
                })
            })
        },

        consultar_producto_almacen() {
            return new Promise((resolve) => {
                this.$http.get('almacen.all').then((response) => {
                    resolve(response.body)
                }).catch(error => {
                    console.log('error consultar_producto_almacen', error)
                })
            })
        },


        actualiza_costo_envio(costo_envio) {
            return new Promise((resolve) => {
                const payload = {
                    costo_envio: costo_envio
                }
                this.$http.post('actualiza.costo.envio', payload).then((response) => {
                    console.log('response', response.body);
                    resolve(true)
                }).catch(error => {
                    reject(false)
                    console.log('error consultar_maquiladores', error)
                })
            })
        },

        // ***********************************************************************//
        exportToExcel(data, nombre_archivo) {
            console.log('datos a exportar', data);
            const ws = XLSX.utils.json_to_sheet(data);
            const wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "Hoja1");

            const wbout = XLSX.write(wb, { bookType: "xlsx", type: "binary" });
            const blob = new Blob([this.s2ab(wbout)], { type: "application/octet-stream" });
            const url = URL.createObjectURL(blob);

            const a = document.createElement("a");
            a.href = url;
            a.download = nombre_archivo + '_' + this.traerFechaActual() + '.xlsx'
            a.click();

            // Libera el objeto URL
            URL.revokeObjectURL(url);
        },

        // Función auxiliar para convertir cadena a matriz de bytes
        s2ab(s) {
            const buf = new ArrayBuffer(s.length);
            const view = new Uint8Array(buf);
            for (let i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xff;
            return buf;
        }


    }
}