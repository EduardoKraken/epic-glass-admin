import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import vuetify from '@/plugins/vuetify';

export default {
    namespaced: true,
    state: {
        login: false,
        datosUsuario: '',
        idusuariosweb: '',
    },

    mutations: {
        LOGEADO(state, value) {
            state.login = value
        },
        DATOS_USUARIO(state, datosUsuario) {
            state.datosUsuario = datosUsuario
        },
        ID_USUARIO(state, idusuariosweb) {
            state.idusuariosweb = idusuariosweb
        },

        SALIR(state) {
            state.login = false
            state.datosUsuario = ''
            state.idusuariosweb = ''
        }
    },

    actions: {
        // Valida si el usario existe en la BD
        validarUser({ commit }, usuario) {
            return new Promise((resolve, reject) => {
                // console.log (usuario)
                Vue.http.post('sessions', usuario).then(respuesta => {
                    return respuesta.json()
                }).then(respuestaJson => {
                    console.log('respuestaJson',respuestaJson)
                    if (respuestaJson == null) {
                        resolve(false)

                    } else {
                        resolve(respuestaJson)
                    }
                }, error => {
                    reject(error)
                })
            })
        },

        GetInfoUser({ commit, dispatch }, usuario) {
            return new Promise((resolve, reject) => {
                Vue.http.post('sessions', usuario).then(response => {
                    console.log('response', response);
                    
                    // Guardar el token en localStorage
                    const token = response.body.access_token;
                    if (token) {
                        localStorage.setItem('EPIC_TOKEN', token);
                    }

                    commit('DATOS_USUARIO', response.body.usuario)
                    commit('LOGEADO', true)
                    resolve(true)
                }, error => {
                    console.log('error', error);
                    reject(error)
                })
            })
        },



        salirLogin({ commit }) {
            commit('SALIR')
        },
    },

    getters: {
        getLogeado(state) {
            return state.login
        },
        getdatosUsuario(state) {
            return state.datosUsuario
        },

        getidUsuariosWeb(state) {
            return state.idusuariosweb
        },

    }
}