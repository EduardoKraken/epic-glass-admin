import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/es5/util/colors'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import es from 'vuetify/es5/locale/es';

Vue.use(Vuetify);

export default new Vuetify({
    lang: {
        locales: { es },
        current: 'es'
    },

    icons: {
        iconfont: 'mdi',
    },
    theme: {
        themes: {
            light: {
                primary: colors.purple.lighten1, // #E53935
                secondary: colors.pink.lighten3, // #FFCDD2
                accent: colors.indigo.base, // #3F51B5
                error: colors.red.darken1,
                info: colors.orange,
                success: colors.green.darken2,
                warning: colors.teal
            },
        },
    },
});