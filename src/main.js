import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import moment from 'moment'
import VueResource from 'vue-resource'

moment.locale('es');
Vue.config.productionTip = false
Vue.prototype.moment = moment
Vue.use(VueResource)

// Vue.http.options.root = 'https://sofsolution.club/ky-fetish/'   // ROOT PARA PODUCCON 
Vue.http.options.root = 'http://localhost:3002/' // ROOT PARA PODUCCON 
// Vue.http.options.root = 'https://sofsolution.club/epicgrass/'   // ROOT PARA PODUCCON 


// Vue.http.interceptors.push((request, next) => {
//     // request.headers.set('Authorization', 'Bearer ' + localStorage.tlaKey)
//     const token = localStorage.getItem('EPIC_TOKEN');
//     console.log('Token:', token);  // Depuración para asegurarte de que el token se encuentra en localStorage

//     request.headers.set('Authorization', 'Bearer ' + token);
//     request.headers.set('Accept', 'application/json')
//     next()
// });

new Vue({
    router,
    store,
    vuetify,
    render: function(h) { return h(App) }
}).$mount('#app')