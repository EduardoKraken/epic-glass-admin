import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/Login/Home.vue'
import Login from '@/views/Login/Login.vue'
import store from '@/store'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: '',
    base: process.env.BASE_URL,
    routes: [
        //MODULO DE LOGIN
        {
            path: '/home',
            name: 'home',
            component: Home,
            meta: { ADMIN: true, USUARIO: true, SERV: true, MAQ: true, REP: true }
        }, {
            path: '/',
            name: 'Login',
            component: Login,
            meta: { libre: true }
        }, {
            path: '/registro',
            name: 'registro',
            component: () =>
                import ('@/views/Login/Registro.vue'),
            meta: { libre: true }
        }, {
            path: '/olvidacontra',
            name: 'olvidacontra',
            component: () =>
                import ('@/views/Login/OlvidaContra.vue'),
            meta: { libre: true }
        }, {
            path: '/cambiacontra/:id',
            name: 'cambiacontra',
            component: () =>
                import ('@/views/Login/CambiaContra.vue'),
            meta: { libre: true }
        }, {
            path: '/activarusuario/:id',
            name: 'activarusuario',
            component: () =>
                import ('@/views/Login/ActivarUsuario'),
            meta: { libre: true }
        },
        // Productos
        {
            path: '/catproductos',
            name: 'catproductos',
            component: () =>
                import ('@/views/clasificacion/arts/CatProductos.vue'),
            meta: { ADMIN: true }
        },
        {
            path: '/tabproducto',
            name: 'tabproducto',
            component: () =>
                import ('@/views/clasificacion/arts/TabProducto.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/newproducto',
            name: 'newproducto',
            component: () =>
                import ('@/views/clasificacion/arts/NewProducto.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/catlab',
            name: 'catlab',
            component: () =>
                import ('@/views/clasificacion/laboratorios/CatLab.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/subcatego',
            name: 'subcatego',
            component: () =>
                import ('@/views/clasificacion/laboratorios/SubCategoria.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/familias',
            name: 'familias',
            component: () =>
                import ('@/views/clasificacion/laboratorios/Familias.vue'),
            meta: { ADMIN: true }
        },
       
       
        {
            path: '/catusuarios',
            name: 'catusuarios',
            component: () =>
                import ('@/views/usuarios/CatUsuarios.vue'),
            meta: { ADMIN: true }
        }, 
      
        {
            path: '/catentradas',
            name: 'catentradas',
            component: () =>
                import ('@/views/almacen/entradas/CatEntradas.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/catsalidas',
            name: 'catsalidas',
            component: () =>
                import ('@/views/almacen/salidas/CatSalidas.vue'),
            meta: { ADMIN: true }
        },
        {
            path: '/catexistencia',
            name: 'catexistencia',
            component: () =>
                import ('@/views/almacen/existencias/CatExistencia.vue'),
            meta: { ADMIN: true }
        },
       
        {
            path: '/banners',
            name: 'banners',
            component: () =>
                import ('@/views/marketing/Banners.vue'),
            meta: { ADMIN: true }
        },
        {
            path: '/cupones',
            name: 'Cupones',
            component: () =>
                import ('@/views/marketing/Cupones.vue'),
            meta: { ADMIN: true }
        },
        {
            path: '/pedidos_pendientes',
            name: 'pedidos_pendientes',
            component: () =>
                import ('@/views/administrativo/Repartidores/pedidos_pendientes.vue'),
            meta: { ADMIN: true, SERV: false, MAQ: false, REP: true }
        }, {
            path: '/pedidos',
            name: 'pedidos',
            component: () =>
                import ('@/views/administrativo/Pedidos/Pedidos.vue'),
            meta: { ADMIN: true, SERV: true }
        }, {
            path: '/maquilaciones',
            name: 'maquilaciones',
            component: () =>
                import ('@/views/administrativo/Maquiladores/Maquilaciones.vue'),
            meta: { ADMIN: true, SERV: false, MAQ: true, REP: false }
        }, {
            path: '/control_entrega',
            name: 'control_entrega',
            component: () =>
                import ('@/views/administrativo/Repartidores/control_entrega.vue'),
            meta: { ADMIN: true, REP: true }
        }, 
        // {
        //     path: '/verpedido',
        //     name: 'verpedido',
        //     component: () =>
        //         import ('@/views/administrativo/VerPedido.vue'),
        //     meta: { ADMIN: true }
        // },
        {
            path: '/sabores',
            name: 'Sabores',
            component: () =>
                import ('@/views/clasificacion/caracteristicas/Sabores.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/tipos',
            name: 'Tipos',
            component: () =>
                import ('@/views/clasificacion/caracteristicas/Tipos.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/aromas',
            name: 'Aromas',
            component: () =>
                import ('@/views/clasificacion/caracteristicas/Aromas.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/efectos',
            name: 'Efectos',
            component: () =>
                import ('@/views/clasificacion/caracteristicas/Efectos.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/presentaciones',
            name: 'Presentaciones',
            component: () =>
                import ('@/views/clasificacion/caracteristicas/Presentaciones.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/marcas',
            name: 'Marcas',
            component: () =>
                import ('@/views/clasificacion/caracteristicas/Marcas.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/olores',
            name: 'Olores',
            component: () =>
                import ('@/views/clasificacion/caracteristicas/Olores.vue'),
            meta: { ADMIN: true }
        }, {
            path: '/unidades',
            name: 'Unidades',
            component: () =>
                import ('@/views/clasificacion/caracteristicas/Unidades.vue'),
            meta: { ADMIN: true }
        },
    ]
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.libre)) {
        next()
    } else if (store.state.Login.datosUsuario.idniveles == 1 ) { // 'ADMIN'
        if (to.matched.some(record => record.meta.ADMIN)) {
            next()
        }
    } else if (store.state.Login.datosUsuario.idniveles == 2 ) { // 'SERV'
        if (to.matched.some(record => record.meta.SERV)) {
            next()
        }
    } else if (store.state.Login.datosUsuario.idniveles == 3 ) { // 'MAQ'
        if (to.matched.some(record => record.meta.MAQ)) {
            next()
        }
    } else if (store.state.Login.datosUsuario.idniveles == 4 ) { // 'REP'
        if (to.matched.some(record => record.meta.REP)) {
            next()
        }
    } else {
        next({
            name: 'Login'
        })
    }
})

export default router