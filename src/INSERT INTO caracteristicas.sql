INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(1, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CHOCOLATE'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CAFÉ'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'KUSH'), 43, 22, 14, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(2, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'EXCITEMENT'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'PASTEL DE FRUTAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'KUSH'), 39, 17, 1, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(3, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'LIMON'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'GH'), 20, 19, 0, 1, 8);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(4, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TALKER'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'PERA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'LAVANDA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'GH'), 40, 15, 1, 0, 7);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(5, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRESCO/AJO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICA'), (SELECT idolores
    FROM olores
    WHERE olor = 'PINO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'GH'), 56, 21, 1, 1, 2);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(6, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICA'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'GH'), 37, 18, 009, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(7, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SMILING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRUTAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'GHP'), 5, 21, 1, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(8, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'IN FOCUS'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'GAS/ FRUTA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'GHP'), 15, 24, 1, 1, 9);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(9, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'AJO/GAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'PINO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'GHP'), 41, 22, 2, 1, 1);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(10, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CHOCOLATE'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CAFÉ'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'GHP'), 36, 21, 1, 0, 15);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(11, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'AZUCAR '), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'HHH'), 2, 17, 0, 0, 8);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(12, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'EUPHORIA'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'VAINILLA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'HHH'), 11, 15,1 , 1, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(13, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CEREZA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'HHH'), 13, 21, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(14, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'LECHE DE MELON'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'HHH'), 10, 25, 4, 8, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(15, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRESCO/PINO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'PINO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'HHH'), 11, 22, 1, 0, 55);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(16, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TINGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'LIMON'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'HERBAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'HHH'), 12, 27, 0, 1, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(17, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TINGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'BERRYAPPLE'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'APPLE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'HHH'), 19, 26, 0, 1, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(18, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'VAINILLA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CAFÉ'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 22, 2, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(19, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'NARANJA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 10, 22, 1, 1, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(20, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TINGLY/GIGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'UVA/AJO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'AJO/CEBOLLA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'LCP'), 0, 24, 1, 1, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(21, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY/ RELAXED'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CACAHUATE'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'NUEZ'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'LCP'), 0, 2, 1, 1, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(22, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY/ CREATIVE'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'GAS/ FRUTA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'COMBUSTIBLE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'LCP'), 0, 22, 1, 1, 2);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(23, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/GIGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'BEERY/UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'LCP'), 10, 21, 0, 0, 7);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(24, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TINGLY/RELAXED'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'NUEZ'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'ZORRILLO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'ANONYMUS'), 0, 24, 0, 0, 2);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(25, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY/SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'BLUE CHEESE'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CHEESE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'ANONYMUS'), 8, 18, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(26, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'BLUE BERRY'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'ANONYMUS'), 8, 17, 1, 1, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(27, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MENTA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'ANONYMUS'), 0, 22, 0, 0, 1);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(28, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MANGO/SPICY'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'ANONYMUS'), 0, 17, 1, 1, 95);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(29, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'BEERY/UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'VIOLETAS'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'ANONYMUS'), 10, 17, 0, 0, 35);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(30, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY/RELAXED'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CITRICO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRESCO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'TIO'), 9, 17, 1, 1, 35);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(31, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY/HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'HONEY/VAINILLA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'TIO'), 16, 18, 1, 1, 45);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(32, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ '), (SELECT idmarca
    FROM marcas
    WHERE marca = 'TIO'), 6, 2, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(33, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'EUPHORIA'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MIEL/FRESCO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'TIO'), 13, 22, 1, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(34, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'UVA/MORA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'TIO'), 15, 17, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(35, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CITRICO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'NARANJA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ '), (SELECT idmarca
    FROM marcas
    WHERE marca = 'TIO'), 15, 17, 0, 0, 85);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(36, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TALKER'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRESA/MENTA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRESCO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ '), (SELECT idmarca
    FROM marcas
    WHERE marca = 'TIO'), 19, 19, 0, 1, 55);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(37, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'NARANJA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRITICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ '), (SELECT idmarca
    FROM marcas
    WHERE marca = 'DOE'), 10, 24.96, 7, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(38, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'NARANJA/LIMON'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'HERBAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ '), (SELECT idmarca
    FROM marcas
    WHERE marca = 'DOE'), 0, 2, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(39, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRESA '), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRESCO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'DOE'), 0, 19, 1, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(40, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRUTO ROJOS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTOS'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ '), (SELECT idmarca
    FROM marcas
    WHERE marca = 'DOE'), 0, 18, 1, 1, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(41, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRUTOS ROJOS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRESCO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ '), (SELECT idmarca
    FROM marcas
    WHERE marca = 'DOE'), 0, 21, 0, 0, 22);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(42, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC/HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRESA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = '1/4 OZ '), (SELECT idmarca
    FROM marcas
    WHERE marca = 'DOE'), 0, 2, 15, 1, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(43, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'DURAZNO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 4, 6, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(44, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'EUPHORIA'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MANZANA VERDE'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 0, 7, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(45, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING/HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MANGO/SPICY'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 4, 8, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(46, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TALKER'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'GALLETAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'GALLETAS'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 6, 5, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(47, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY/HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRESAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 6, 9, 0, 0, 65);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(48, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'BANANA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 4, 9, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(49, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING/HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'COMBUSTIBLE'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'AJO/CEBOLLA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 0, 5, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(50, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'DULCES'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'HERBAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 0, 7, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(51, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'PAPAYA/MELON'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 9, 6, 0, 0, 45);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(52, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING/HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'TROPICAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 5, 8, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(53, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY/HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'NARANJA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 6, 7, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(54, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'NARANJA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 2, 9, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(55, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MORA AZUL'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 0, 8, 0, 0, 45);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(56, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'COCO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 5, 88, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(57, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'BANANA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD '), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 1, 85, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(58, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'HELADO FRUTAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 0, 75, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(59, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'ALGODÓN DE AZUCAR'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 1, 7, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(60, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MENTOLADO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 3, 6, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(61, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'SANDIA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 0, 9, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(62, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TINGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MORAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'MORA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 1, 9, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(63, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'GIGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRUTAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'TROPICAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 0, 9, 0, 0, 55);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(64, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/TINGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MIEL/KIWI'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'KIWI'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 2, 8, 0, 0, 45);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(65, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'PIÑA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'TROPICAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 2, 7, 0, 0, 65);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(66, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING/HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'DURAZNO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 2, 5, 0, 0, 55);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(67, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'EUPHORIA'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CARAMELO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 1, 6, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(68, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY/HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MELON'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAS'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 4, 9, 0, 0, 35);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(69, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CHICLE'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 3, 7, 0, 0, 22);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(70, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRESA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 3, 8, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(71, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'UVA/LIMON'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 2, 7, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(72, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MORA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'MORA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 3, 7, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(73, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TINGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'GASEOSA UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'UVA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BUZZBAR'), 3, 7, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(74, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRUTAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BOOMBARS'), 10, 8, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(75, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'SANDIA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BOOMBARS'), 3, 9, 0, 0, 25);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(76, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TINGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'LIMON/CEREZA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BOOMBARS'), 8, 7, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(77, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRESA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BOOMBARS'), 5, 85, 0, 0, 1);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(78, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'GIGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'HIERBA FRESCA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'HERBAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BOOMBARS'), 9, 75, 0, 0, 25);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(79, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'EUPHORIA'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'PIÑA/FRESA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'TROPICAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BOOMBARS'), 7, 65, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(80, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'LIMON'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BOOMBARS'), 10, 6, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(81, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'LIMON/UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BOOMBARS'), 8, 5, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(82, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'UVA/MORA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'MORA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BOOMBARS'), 10, 9, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(83, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'TAMARINDO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'SPICY'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BOOMBARS'), 10, 85, 0, 0, 35);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(84, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRUTAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRUTAS'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'CAKE'), 1, 88, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(85, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MORA/UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'MORA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'CAKE'), 0, 86, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(86, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MORAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'CAKE'), 15, 87, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(87, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/SLEEPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'ALGODÓN DE AZUCAR'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'WHOLEMELT'), 6, 9, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(88, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY/HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CARAMELO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'WHOLEMELT'), 4, 75, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(89, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/GIGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'BERRY/UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'LCP'), 7, 9, 0, 0, 7);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(90, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TINGGLY/GIGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'UVA/AJO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'AJO/CEBOLLA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'LCP'), 4, 85, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(91, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/CREATIVE'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'GAS/FRUTA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'COMBUSTIBLE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'POD'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'LCP'), 8, 7, 0, 0, 2);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(92, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRUTA TROPICAL'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLLS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BABY JEETER'), 0, 3507, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(93, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRESCO/FRUTAL'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLLS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BEBY JEETER'), 0, 3712, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(94, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'EUPHORIA'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'UVA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLLS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BABY JEETER'), 0, 3736, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(95, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'ZARZAMORA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'BERRY'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLLS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BABY JEETER'), 1, 4238, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(96, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TINGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'PERA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'PERA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLLS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BABY JEETER'), 1, 4306, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(97, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'PIÑA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'PIÑA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLLS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BABY JEETER'), 1, 4853, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(98, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY/HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'HELADO FRUTAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLLS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BABY JEETER'), 0, 351, 0, 0, 7);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(99, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'HERBAL'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRESCO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'DONUT'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'FROZEN'), 12, 4005, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(100, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'COMBUSTIBLE'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'HERBAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'DONUT'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'FROZEN'), 15, 3956, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(11, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'EUPHORIA'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'GAS/UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'DONUT'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'FROZEN'), 2, 4106, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(12, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'FRUTAS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'DONUT'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'FROZEN'), 3, 422, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(103, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'KUSH'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'ZORRILLO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'DONUT'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'FROZEN'), 9, 431, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(104, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CEREZA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLL '), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BACKPACK'), 5, 1949, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(105, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'HERBAL'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRESCO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLL'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BACKPACK'), 4, 221, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(106, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'RELAXING'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CHICLE'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLL'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BACKPACK'), 3, 1825, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(107, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'TINGGLY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'BERRY'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLL'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BACKPACK'), 4, 1756, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(108, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'SKUNK'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRESCO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLL'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BACKPACK'), 3, 1933, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(109, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'EUPHORIA'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'HORCHATA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLL'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BACKPACK'), 2, 201, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(110, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CITRICO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'SATIVO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLL'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BACKPACK'), 5, 162, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(111, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'HERBAL'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'HERBAL'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLL'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BACKPACK'), 4, 15.05, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(112, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/CREATIVE'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'LIMON/CHERRY'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLL'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BACKPACK'), 3, 21.05, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(113, 
      (SELECT idefectos FROM efectos WHERE efecto = 'SLEEPY/HAPPY'), 
      (SELECT idsabores FROM sabores WHERE sabor = 'CHERRY'), 
      (SELECT idtipos   FROM tipos   WHERE tipo = 'HIBRYDO'), 
      (SELECT idolores  FROM olores  WHERE olor = 'DULCE'), 
      (SELECT idpresentaciones FROM presentaciones  WHERE presentacion = 'PREROLL'), 
      (SELECT idmarca  FROM marcas  WHERE marca = 'BACKPACK'), 
      2, 2345, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(114, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'PINO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'ZORRILLO '), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'PREROLL'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'BACKPACK'), 3, 22.1, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(115, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CARAMELO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'DONUT'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 6, 18, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(116, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CEREZA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'VAINILLA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'DONUT'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 1, 22, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(117, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MELON'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'UVA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'DONUT'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 2, 25, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(118, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'UVA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'AJO/CEBOLLA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'BABYS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 1, 24, 0, 0, 6);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(119, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CEREZA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'VAINILLA'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'BABYS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 1, 22, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(120, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'EUPHORIA'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MENTA'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'INDICO'), (SELECT idolores
    FROM olores
    WHERE olor = 'FRESCO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'BABYS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 5, 2, 0, 0, 3);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(121, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CARAMELO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'BABYS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 6, 18, 0, 0, 5);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(122, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'ENERGETIC'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'PLATANO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'DULCE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'BABYS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'EPIC'), 9, 16, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(123, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'RICE KRISPIES'), (SELECT idtipos
    FROM tipos
    WHERE tipo = '*'), (SELECT idolores
    FROM olores
    WHERE olor = 'CHOCOLATE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'CHOCOHONGO'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'POLKADOT'), 7, 0, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(124, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CINNAMON'), (SELECT idtipos
    FROM tipos
    WHERE tipo = '*'), (SELECT idolores
    FROM olores
    WHERE olor = 'CHOCOLATE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'CHOCOHONGO'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'POLKADOT'), 7, 0, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(125, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'STRAWBERRY'), (SELECT idtipos
    FROM tipos
    WHERE tipo = '*'), (SELECT idolores
    FROM olores
    WHERE olor = 'CHOCOLATE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'CHOCOHONGO'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'POLKADOT'), 5, 0, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(126, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'MILANO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = '*'), (SELECT idolores
    FROM olores
    WHERE olor = 'CHOCOLATE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'CHOCOHONGO'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'POLKADOT'), 8, 0, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(127, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPRY/HUNGRY'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'LUCKY CHARMS'), (SELECT idtipos
    FROM tipos
    WHERE tipo = '*'), (SELECT idolores
    FROM olores
    WHERE olor = 'CHOCOLATE'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'CHOCOHONGO'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'POLKADOT'), 6, 0, 0, 0, 4);
INSERT INTO caracteristicas
  (id_articulo, id_efecto, id_sabor, id_tipo, id_olor, id_presentacion, id_marca, cantidad, thc, cbd, cbg, energia)
VALUES(128, (SELECT idefectos
    FROM efectos
    WHERE efecto = 'HAPPY/RELAX'), (SELECT idsabores
    FROM sabores
    WHERE sabor = 'CITRICO/FRESCO'), (SELECT idtipos
    FROM tipos
    WHERE tipo = 'HIBRYDO'), (SELECT idolores
    FROM olores
    WHERE olor = 'CITRICO'), (SELECT idpresentaciones
    FROM presentaciones
    WHERE presentacion = 'MINI PREROLLS'), (SELECT idmarca
    FROM marcas
    WHERE marca = 'NECTE JOINTZ'), 17, 2903, 9, 0, 5);
